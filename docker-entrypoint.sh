#!/bin/bash
#
endfile="/var/www/foswiki/working/logs/installation.end"
if [ -f $endfile ] ; then
    rm $endfile
fi
#

echo "...rsync Foswiki from tmp to the production /var/www/foswiki"
rsync -avh /tmp/foswiki/* /var/www/foswiki
sed -i "s|/tmp/foswiki|/var/www/foswiki|g" /var/www/foswiki/lib/LocalSite.cfg
echo "...configure Foswki for CERN with OpenID and egroup integration"
 
if [[ ! -e /var/www/foswiki/data/.htpasswd ]]; then
    touch /var/www/foswiki/data/.htpasswd
fi

#
echo "...preparing Solr for Foswiki"

cd /opt/solr/server/solr/configsets
[ -L foswiki_configs ] && rm foswiki_configs
ln -s /var/www/foswiki/solr/configsets/foswiki_configs/ .

cd /opt/solr/server/solr/solr_foswiki
[ -L core.properties ] && rm core.properties
ln -s /var/www/foswiki/solr/cores/foswiki/core.properties

sed -i '/SolrPlugin..Url/s/localhost/solr/' /var/www/foswiki/lib/LocalSite.cfg

echo "...enabling NatSkin as the default look and feel"
grep -q "Set SKIN = nat" /var/www/foswiki/data/Main/SitePreferences.txt || sed -i '/---++ Appearance/a\ \ \ * Set SKIN = nat' /var/www/foswiki/data/Main/SitePreferences.txt

echo "Update the LocalSite.cfg with Application name = ${APPLICATION_NAME} , Application admin = ${ADMIN_USERNAME}"

FOSWIKIUSEREMAIL=$(/usr/bin/ldapsearch -x -h xldap.cern.ch -b 'OU=Users,OU=Organic Units,DC=cern,DC=ch' '(&(objectClass=user) (sAMAccountName='${ADMIN_USERNAME}'))' | awk -F ": " '$1 == "mail" {print $2}' | sed 's/ //g')

echo "The WebMasterEmail is now set to ${FOSWIKIUSEREMAIL} , goto bin/configure to change this"

sed "s/APPLICATION_NAME/${APPLICATION_NAME}/g" -i  /var/www/foswiki/lib/LocalSite.cfg
sed "s/ADMIN_EMAIL/${FOSWIKIUSEREMAIL}/g" -i  /var/www/foswiki/lib/LocalSite.cfg
sed "s/OIDC_CLIENT_ID/${OIDC_CLIENT_ID}/g" -i  /var/www/foswiki/lib/LocalSite.cfg
sed "s/OIDC_CLIENT_SECRET/${OIDC_CLIENT_SECRET}/g" -i  /var/www/foswiki/lib/LocalSite.cfg
#Apply the new password
cd /var/www/foswiki
tools/configure -save -set {Password}=${ADMIN_PASSWORD}
#
#If there is no OIDC_CLIENT_SECRET then rest the PASSWORD Manager
secretlength=${OIDC_CLIENT_SECRET}
if [ ${#secretlength} -le 15 ]
then
      tools/configure -save -set {LoginManager}='Foswiki::LoginManager::TemplateLogin';
      tools/configure -save -set {PasswordManager}='Foswiki::Users::HtPasswdUser'
else
      echo "OIDC data has been set. Instance configured for OpenID login"
fi
#

#Add the CERN account of the site administrator
FOSWIKIUSER=$(/usr/bin/ldapsearch -x -h xldap.cern.ch -b 'OU=Users,OU=Organic Units,DC=cern,DC=ch' '(&(objectClass=user) (sAMAccountName=${ADMIN_USERNAME}))' | awk -F ": " '$1 == "displayName" {print $2}' | sed 's/ //g')
echo ${FOSWIKIUSER}

sed -i "s|value=\"\"|value=\"PeteJones\"|" /var/www/foswiki/data/Main/AdminGroup.txt
tools/configure -save -set {FeatureAccess}{Configure}="PeteJones ${FOSWIKIUSER}"

touch /var/www/foswiki/working/logs/installation.end

echo "...starting nginx"

nginx -g "daemon off;"
