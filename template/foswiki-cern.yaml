---
kind: Template
apiVersion: v1
metadata:
  name: foswiki-cern
  annotations:
    openshift.io/display-name: foswiki (CERN)
    description: Creates a Foswiki instance.
      One persistent volumes for Foswiki data and the Solr search service is required.
    tags: foswiki
    iconClass: perl
message: "This Foswiki instance will be ready to use in a couple of minutes."
parameters:
- name: APPLICATION_NAME
  description: The name of the foswiki instance, must be EXACTLY the web sitename you registered, used in http://sitename.web.cern.ch and http://cern.ch/sitename.
  value: "foswiki-cern"
  from: "[a-zA-Z0-9]"
  required: true
- name: OIDC_CLIENT_SECRET
  description: For use with the CERN SSO OpenID authentication. Can be configured later
  value: "123456"
  from: "[ -~]"
  required: false
- name: ADMIN_USERNAME
  description: The CERN user account of the site administrator 
  value: "pete"
  from: "[a-zA-Z0-9]"
  required: true
- name: ADMIN_PASSWORD
  description: The internal Foswiki admin password. Note this for future use if ever needed.
  value: "foswiki"
  from: "[a-zA-Z0-9]"
  required: false
objects:
- kind: ImageStream
  apiVersion: v1
  metadata:
    creationTimestamp: null
    labels:
      solr.service: solr
    name: solr
  spec:
    tags:
    - annotations: null
      from:
        kind: DockerImage
        name: solr:5
      generation: null
      importPolicy: {}
      name: "5"
  status:
    dockerImageRepository: ""
- kind: ImageStream
  apiVersion: v1
  metadata:
    labels:
      app: foswiki-cern
    name: foswiki-cern
    namespace: foswiki-cern
  spec:
    lookupPolicy:
      local: false
    tags:
    - annotations:
        openshift.io/generated-by: OpenShiftWebConsole
        openshift.io/imported-from: gitlab-registry.cern.ch/pete/foswiki-cern:latest
      from:
        kind: DockerImage
        name: gitlab-registry.cern.ch/pete/foswiki-cern:latest
      importPolicy: {}
      name: latest
      referencePolicy:
        type: Source
- kind: DeploymentConfig
  apiVersion: v1
  metadata:
    labels:
      app: foswiki-cern
    name: foswiki-cern
    namespace: foswiki-cern
  spec:
    strategy:
      type: Recreate
    triggers:
    - type: ConfigChange
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
          - foswiki-cern
        from:
          kind: ImageStreamTag
          name: 'foswiki-cern:latest'
    - type: ConfigChange
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
          - foswiki-fcgi
        from:
          kind: ImageStreamTag
          name: 'foswiki-cern:latest'
    replicas: 1
    selector:
      app: foswiki-cern
      deploymentconfig: foswiki-cern
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        creationTimestamp: null
        labels:
          app: foswiki-cern
          deploymentconfig: foswiki-cern
      spec:
        containers:
        - name: foswiki-cern
          image: gitlab-registry.cern.ch/pete/foswiki-cern:latest
          imagePullPolicy: Always        
          ports:
          - containerPort: 8080
            protocol: TCP 
          resources:         
            limits:
              cpu: 450m
              memory: 950Mi
            requests:
              cpu: 350m
              memory: 500Mi
          env:
          - name: APPLICATION_NAME
            value: "${APPLICATION_NAME}"
          - name: OIDC_CLIENT_SECRET
            value: "${OIDC_CLIENT_SECRET}"
          - name: ADMIN_USERNAME
            value: "${ADMIN_USERNAME}"
          - name: ADMIN_PASSWORD
            value: "${ADMIN_PASSWORD}" 
          volumeMounts:
          - mountPath: /data
            name: foswiki-www
          - mountPath: /opt/solr/server/solr/configsets
            name: foswiki-www 
            subPath: configsets 
          - mountPath: /opt/solr/server/solr/solr_foswiki
            name: foswiki-www
            subPath: solr-foswiki
          - mountPath: /opt/solr/server/logs
            name: foswiki-www 
            subPath: solr-logs
          - mountPath: /var/www/foswiki
            name: foswiki-www
            subPath: foswiki
          restartPolicy: Always
        - name: foswiki-fcgi
          image: gitlab-registry.cern.ch/pete/foswiki-cern:latest
          command: ['bash', '-c', "date; until [ -f /var/www/foswiki/working/logs/installation.end ]; do sleep 10; echo Wait for all Foswiki files; done; echo Start 5 instances of fcgi in the foreground; cd /var/www/foswiki/bin/; ./foswiki.fcgi -l 127.0.0.1:9000 -n 5;  sleep 9999999999d & wait"]
          imagePullPolicy: IfNotPresent 
          resources:         
            limits:
              cpu: 450m
              memory: 950Mi
            requests:
              cpu: 350m
              memory: 500Mi
          volumeMounts:
          - mountPath: /data
            name: foswiki-www
          restartPolicy: Always
        - name: solr
          image: solr:5
          ports:
          - containerPort: 8983
          imagePullPolicy: Always 
          volumeMounts:
          - mountPath: /data
            name: foswiki-www
          - mountPath: /opt/solr/server/solr/configsets
            name: foswiki-www 
            subPath: configsets 
          - mountPath: /opt/solr/server/logs
            name: foswiki-www 
            subPath: solr-logs
          - mountPath: /opt/solr/server/solr/solr_foswiki
            name: foswiki-www
            subPath: solr-foswiki
          - mountPath: /var/www/foswiki
            name: foswiki-www
            subPath: foswiki
          restartPolicy: Always      
        initContainers:
        - name: init-foswiki-cern-directories
          image: gitlab-registry.cern.ch/pete/foswiki-cern:latest
          command: ['sh', '-c', 'mkdir -p /data/foswiki /data/configsets /data/solr-foswiki /data/solr-logs']
          volumeMounts:
          - mountPath: /data
            name: foswiki-www
        - name: init-foswiki-cern-entrypoint
          image: gitlab-registry.cern.ch/pete/foswiki-cern:latest
          command: ['sh', '-c', 'date;  rsync -ah /tmp/foswiki/* /data/foswiki; sed -i "s|/tmp/foswiki|/var/www/foswiki|g" /var/www/foswiki/lib/LocalSite.cfg; cd /opt/solr/server/solr/configsets; [ -L foswiki_configs ] && rm foswiki_configs; ln -s /var/www/foswiki/solr/configsets/foswiki_configs/ . ; cd /opt/solr/server/solr/solr_foswiki; [ -L core.properties ] && rm core.properties; ln -s /var/www/foswiki/solr/cores/foswiki/core.properties; date']
          volumeMounts:
          - mountPath: /data
            name: foswiki-www
          imagePullPolicy: IfNotPresent
        volumes:
        - name: foswiki-www
          persistentVolumeClaim:
            claimName: foswiki-www
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
  status: {}
- kind: PersistentVolumeClaim
  apiVersion: v1
  metadata:
    name: "foswiki-www"
    labels:
      app: "${APPLICATION_NAME}-foswiki-www"
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: 5Gi
- kind: Service
  apiVersion: v1
  metadata:
    name: ${APPLICATION_NAME}-service
    labels:
      app: ${APPLICATION_NAME}
  spec:
    ports:
    - name: 8080-tcp
      protocol: TCP
      port: 8080
      targetPort: 8080
    selector:
      app: foswiki-cern
      deploymentconfig: foswiki-cern
- kind: Route
  apiVersion: v1
  metadata:
    name: my-${APPLICATION_NAME}-route
    labels:
      app: ${APPLICATION_NAME}
  spec:
    to:
      kind: Service
      name: ${APPLICATION_NAME}-service
    port:
      targetPort: 8080-tcp
    tls:
      termination: edge
      insecureEdgeTerminationPolicy: Redirect
- kind: CronJob
  apiVersion: batch/v1beta1
  metadata:
    name: tick
  spec:
    successfulJobsHistoryLimit: 1
    failedJobsHistoryLimit: 1
    schedule: "0 0 * * 0"
    jobTemplate:
      spec:
        template:
          spec:
            containers:
            - name: tick
              image: gitlab-registry.cern.ch/pete/foswiki-cern
              volumeMounts:
              - mountPath: /var/www/foswiki
                name: foswiki-www
              args:
              - /bin/sh
              - -c
              - date; echo Hello from Openshift running tick to cleanup sessions; cd /var/www/foswiki/bin && perl  ../tools/tick_foswiki.pl;date
            restartPolicy: OnFailure
            volumes:
            - name: foswiki-www
              persistentVolumeClaim:
               claimName: foswiki-www
- kind: CronJob
  apiVersion: batch/v1beta1
  metadata:
    name: statistics
  spec:
    successfulJobsHistoryLimit: 1
    failedJobsHistoryLimit: 1
    schedule: "0 0 * * *"
    jobTemplate:
      spec:
        template:
          spec:
            containers:
            - name: statistics
              image: gitlab-registry.cern.ch/pete/foswiki-cern
              volumeMounts:
              - mountPath: /var/www/foswiki
                name: foswiki-www
              args:
              - /bin/sh
              - -c
              - date; echo Hello updating statistics;  cd /var/www/foswiki/bin && perl  ./statistics -subwebs 1;date
            restartPolicy: OnFailure
            volumes:
            - name: foswiki-www
              persistentVolumeClaim:
               claimName: foswiki-www
- kind: CronJob
  apiVersion: batch/v1beta1
  metadata:
    name: mailnotifyer
  spec:
    successfulJobsHistoryLimit: 1
    failedJobsHistoryLimit: 1
    schedule: "0 0 * * *"
    jobTemplate:
      spec:
        template:
          spec:
            containers:
            - name: mailnotifyer
              image: gitlab-registry.cern.ch/pete/foswiki-cern
              volumeMounts:
              - mountPath: /var/www/foswiki
                name: foswiki-www
              args:
              - /bin/sh
              - -c
              - date; echo Running the mailnotifier; date; cd /var/www/foswiki/bin && perl  ../tools/mailnotify  ;date
            restartPolicy: OnFailure
            volumes:
            - name: foswiki-www
              persistentVolumeClaim:
               claimName: foswiki-www

